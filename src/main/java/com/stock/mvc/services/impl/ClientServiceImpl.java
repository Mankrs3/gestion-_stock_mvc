package com.stock.mvc.services.impl;

import java.util.List;

import com.stock.mvc.dao.IClientDao;
import com.stock.mvc.entites.Client;
import com.stock.mvc.services.IClientService;

public class ClientServiceImpl implements IClientService {
	IClientDao dao;
	

	public IClientDao getDao() {
		return dao;
	}

	public void setDao(IClientDao dao) {
		this.dao = dao;
	}

	
	public Client save(Client entity) {
		
		return dao.save(entity);
	}

	@Override
	public Client update(Client entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Client> selectAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Client> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Client getById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void remove(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Client findOne(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Client findOne(String[] paramNames, Object[] paramValues) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int findCountBy(String paramName, String paramValue) {
		// TODO Auto-generated method stub
		return 0;
	}

}
