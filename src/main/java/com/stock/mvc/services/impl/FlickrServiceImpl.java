package com.stock.mvc.services.impl;

import java.io.InputStream;

import com.flickr4java.flickr.FlickrException;
import com.stock.mvc.services.IflickrService;

public class FlickrServiceImpl implements IflickrService {
	private IflickrService dao;
	

	public IflickrService getDao() {
		return dao;
	}


	public void setDao(IflickrService dao) {
		this.dao = dao;
	}


	@Override
	public String savePhoto(InputStream photo, String title) throws FlickrException {
		// TODO Auto-generated method stub
		return dao.savePhoto(photo, title);
	}

}
