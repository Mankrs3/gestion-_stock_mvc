package com.stock.mvc.services;

import java.io.InputStream;

import com.flickr4java.flickr.FlickrException;

public interface IflickrService {
	public String savePhoto(InputStream photo, String title) throws FlickrException;
}
