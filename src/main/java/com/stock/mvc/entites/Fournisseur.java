package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class Fournisseur implements Serializable {
	
	@Id
	@GeneratedValue
	private Long idFournisseur;
	private String nom; 
	private String prenom; 
	private String adresse;
	private String photo;
	private String email; 
@OneToMany(mappedBy= "fournisseur")
	private List<CommandeFournisseur> commandeFournisseur;


	public String getNom() { 
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Fournisseur() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public Long getIdFournisseur() {
		return idFournisseur;
	}

	public void setIdFournisseur(long idFournisseur) {
		this.idFournisseur = idFournisseur;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<CommandeFournisseur> getCommandeFournisseur() {
		return commandeFournisseur;
	}

	public void setCommandeFournisseur(List<CommandeFournisseur> commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}

}
